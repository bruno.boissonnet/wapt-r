# -*- coding: utf-8 -*-
from setuphelpers import *

r"""
Usable WAPT package functions: install(), uninstall(), session_setup(), audit(), update_package()

"""

# Declaring global variables - Warnings:
# 1) WAPT context is only available in package functions;
# 2) Global variables are not persistent between calls


def install():
    print("--> DÉBUT DE L'INSTALLATION")

    # Récupère le nom du logiciel
    SoftwareName = control.get('name')
    print(f"Nom du logiciel     : {SoftwareName}.")

    # Récupère le numéro de version du logiciel (sans l'ajout de WAPT)
    SoftwareVersion = control.get_software_version().split('-',1)[0]
    print(f"Version du logiciel : {SoftwareVersion}.")

    # Récupère le nom de l'exécutable d'installation
    bin_name = glob.glob(f"{SoftwareName}-{SoftwareVersion}-win.exe")[0]

    # Installe le logiciel avec les paramètres
    print(f"Nom de l'exécutable : {bin_name}")

    SoftwareKey = f'R for Windows {SoftwareVersion}_is1'

    # https://cran.r-project.org/bin/windows/base/rw-FAQ.html#Can-I-customize-the-installation_003f

    install_exe_if_needed(
        bin_name,
        silentflags='/VERYSILENT /COMPONENTS="main,x64,translations"',
        key=SoftwareKey,
        min_version=SoftwareVersion
    )

    # Supprime le raccourci du bureau (nettoyage)
    SoftwareDesktopShortcut = f'{SoftwareName} {SoftwareVersion}'
    remove_desktop_shortcut(SoftwareDesktopShortcut)

    print("--> FIN DE L'INSTALLATION.")









